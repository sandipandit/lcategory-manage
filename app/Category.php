<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    
    public function childs(){
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

}
