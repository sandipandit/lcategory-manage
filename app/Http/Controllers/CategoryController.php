<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('parent_id', 0)->get();
        $allCategories = [];
        $allCategories = $this->allCategoresName(Category::all()->toArray());
        // dd($allCategories);
        
        return view('category.index', compact('categories', 'allCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        $request->validate([
            'name' => 'required|unique:categories'
        ]);

        $category->name = $request->name;
        $category->description = $request->description;
        $category->parent_id = $request->parent_id;
        $category->save();
        return response([
            'status'=> true,
            'data'=> $category->all()
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        if($id){
            return response($category, 200);
        }
        return response([]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $category = new Category;
            $category = $category->find($id);
            $request->validate([
                'uname' => 'required',

            ]);

            $category->name = $request->uname;
            $category->description = $request->udescription;
            $category->parent_id = $request->uparent_id;
            $category->save();
            return response([
                'status'=> true,
                'data'=> $category
            ], 200);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!empty($id)){
            $category = new Category;
            $category = $category->find($id)->delete();
            return response(['status'=>$category], 200);
        }

        
    }

    public function allCategoresName($allCategories){
        $categories = ['No Parent'];
        foreach($allCategories as $key => $value){
            $categories[$value['id']] = $value['name'];
        }
        return $categories;
    }
}
