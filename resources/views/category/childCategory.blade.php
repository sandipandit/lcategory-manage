<ul>
@foreach($childs as $key => $child)
	<li>
	    {{ $child->name }} <span><a class="text-primary edit" data-id="{{ $childs[$key]['id'] }}" href="Javascript:void(0);"><i class="fa fa-edit"></i></a> <a id="delete" class="text-danger delete" data-id="{{ $childs[$key]['id'] }}" href="Javascript:void(0);"><i class="fa fa-trash"></i></a></span>
	    @if(count($child->childs))
            @include('category.childCategory',['childs' => $child->childs])
        @endif
	</li>
@endforeach
</ul>