@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Manage Category 
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#categoryModal">
                    Add Category
                    </button>
                </div>

                <div class="card-body">
                    <div id="ajax-status"></div>
                    @forelse($categories as $category)
                        <ul>
                            <li>
                                {{ $category->name }} <span><a class="text-primary edit" data-id="{{ $category->id }}" href="Javascript:void(0);"><i class="fa fa-edit"></i></a> <a id="delete" class="text-danger delete" data-id="{{ $category->id }}" href="Javascript:void(0);"><i class="fa fa-trash"></i></a></span>
                                @if( count($category->childs) )
                                    <!-- @foreach( $category->childs as $subCategory )
                                        <ul>
                                            <li> {{ $subCategory->childs }} </li>
                                        </ul>
                                    @endforeach -->
                                    @include('category.childCategory', ['childs'=> $category->childs])
                                @endif
                            </li>
                        </ul>
                    @empty
                        No category found...!    
                    @endforelse
                </div>
            </div>
        </div>

        </div>
    </div>
</div>


<!-- Modal -->
<form method="POST" action="{{ route('category.store') }}" id="form">
<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>

                    <div class="col-md-6">
                        <textarea class="form-control" name="description" id="description"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="parent_id" class="col-md-4 col-form-label text-md-right">Parent</label>

                    <div class="col-md-6">
                        <select class="form-control" name="parent_id" id="parent_id"> 
                            @foreach( $allCategories as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" id="add-category" class="btn btn-primary actionBtn">Save changes</button>
      </div>
    </div>
  </div>
</div>
</form>

<form method="POST" action="" id="updateForm">
<div class="modal fade" id="updateCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                @csrf
                <div class="form-group row">
                    <label for="uname" class="col-md-4 col-form-label text-md-right">Name</label>

                    <div class="col-md-6">
                        <input id="uname" type="text" class="form-control @error('uname') is-invalid @enderror" name="uname" value="{{ old('uname') }}" autofocus>

                        @error('uname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="udescription" class="col-md-4 col-form-label text-md-right">Description</label>

                    <div class="col-md-6">
                        <textarea class="form-control" name="udescription" id="udescription"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="uparent_id" class="col-md-4 col-form-label text-md-right">Parent</label>

                    <div class="col-md-6">
                        <select class="form-control" name="uparent_id" id="uparent_id"> 
                            @foreach( $allCategories as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" id="updataCategory" class="btn btn-primary">Update changes</button>
      </div>
    </div>
  </div>
</div>
</form>
@endsection

@section('style')
    <style>
        p.alert.alert-sucess {
            margin: 0;
            padding: 0;
        }
    </style>
@endsection

@section('scripts')
    <script>
    window.onload = ()=> {
        $(document).ready(()=>{
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#add-category').on('click', (e)=>{
                const data = {
                    name: $('#name').val(),
                    description: $('#description').val(),
                    parent_id: $('#parent_id').val()
                    
                };
                
                e.preventDefault();
                
                const request = $.ajax({
                    url: `${ajaxURL}/category`,
                    method: 'POST',
                    dataType: 'json',
                    data: data,
                    beforeSend: ()=>{

                    },
                    success: (data)=>{
                        const cathtml = `<div class="alert alert-success">
                        <p class="alert alert-sucess">Category Added Successfully...</p>
                    </div>`;
                        $('#ajax-status').html(cathtml);
                        $('#categoryModal').modal('hide');
                        $("#form").trigger('reset');
                    },
                    error: (error)=>{
                        console.log(0);
                    }

                });
            });

            // for(i=0; i<=$('.edit').length; i++){
            //     console.log(i);
            //     $(i).on('click', (e)=>{
            //         console.log(i);
            //         console.log($(this).data("id"));
            //     });
            // }

            // $('.edit').map(function() {
            //     console.log($(this).data('id'));
            // });
            $('body').on('click', '.edit', function(e) {  
                console.log($(this).data('id'));
                const id = $(this).data('id');
                $('#updateCategoryModal').modal('show');
                e.preventDefault();
                $.ajax({
                    url: `${ajaxURL}/category/${id}/edit`,
                    method: 'GET',
                    dataType: 'json',
                    data: {},
                    success: (result)=>{
                        console.log(result);
                        // $('.actionBtn').attr('id', 'updateCatgory');
                        $('#updateCategoryModal #uname').val(result.name);
                        $('#updateCategoryModal #udescription').val(result.description);
                        // $('#updateCategoryModal #uparent_id').val(result.parent_id);
                        $(`#updateCategoryModal #uparent_id option[value=${result.parent_id}]`).attr('selected','selected');
                        update();
                        $('#updataCategory').on('click', (e)=>{
                            e.preventDefault();
                            $.ajax({
                                url: `/category/${id}`,
                                method: 'PUT',
                                dataType: 'json',
                                data: $('#updateForm').serialize(),
                                success: (result)=>{
                                    console.log(result)
                                },
                                error: (error)=>{
                                    console.log(error);
                                }
                            });
                        });
                    },
                    error: (error)=>{
                        console.log(error);
                    }
                });
            });

            $('body').on('click', '.delete', function(e) {  
                console.log($(this).data('id'));
                const id = $(this).data('id');
                $.ajax({
                    url: `${ajaxURL}/category/${id}`,
                    method: 'DELETE',
                    dataType: 'json',
                    data: {id: id},
                    success: (result)=>{
                        document.location.reload();
                    },
                    error: (error)=>{
                        console.log('delete error: ',error);
                    }
                });
            });

            function update(data){
                console.log(data);
            }

        });
    }

    </script>
@endsection